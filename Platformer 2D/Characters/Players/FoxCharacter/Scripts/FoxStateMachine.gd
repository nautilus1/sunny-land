extends "res://Scripts/State Machine/state_machine.gd"

onready var anim_player: AnimationPlayer

func _ready():
	states_map = {
			"Idle":$Idle,
			"Move": $Move,
			"Crouch": $Crouch,
			"Jump": $Jump,
			"Hurt":$Hurt,
			"Climb": $Climb
			}
	
	.set_active(true)
	change_state("Idle")

func change_state(new_state):
	.change_state(new_state)
