extends "res://Scripts/State Machine/state.gd"

func enter():
	owner.get_node("Anim").play("hurt")
	global_var.lifes -= 1

func update(delta):
	move(owner.speed, owner.direction);

func move(speed, direction):
	owner.velocity.x = direction.x * speed;
	owner.move_and_slide(owner.velocity, Vector2(0,-1));

func _on_Anim_animation_finished(anim_name):
	if anim_name == "hurt":
		if global_var.lifes == 0:
			owner.queue_free()
			get_tree().quit()
			return
		emit_signal("finished","Idle")
