extends "res://Characters/Enemies/Enemy.gd"

func _ready():
	direction.y = 1;

func _physics_process(delta):
	._apply_gravity(true,delta);
	
	var t = transform
	t[2].y += 5 if direction.y == 1 else -5
	
	if test_move(t,Vector2(0,5)):
		direction.y = -1;
	elif !test_move(t,Vector2(0,60)):
		direction.y = 1;

func _on_anim_animation_finished(anim_name):
	if anim_name == "Died":
		queue_free();

func _on_DetectCollision_body_entered(body):
	if body.is_in_group("Players") and body.get_node("Anim").current_animation != "hurt":
		body.emit_signal("finished","Hurt");