extends ParallaxLayer

export (float, 10, 200) var background_speed = 50

func _process(delta):
	motion_offset.x += background_speed * delta