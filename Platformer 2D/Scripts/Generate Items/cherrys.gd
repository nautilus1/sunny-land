extends TileMap

var items = {
		"Cherry": load("res://Items/Cherry/Cherry.tscn"),
		"Escalera": load("res://Objects/Escalera/Escalera.tscn")
}

func _ready():
	for c in get_used_cells_by_id(0):
		new_instance(items["Cherry"], c)
	for e in get_used_cells_by_id(1):
		new_instance(items["Escalera"], e)

func new_instance(item, pos):
	var new_item = item.instance()
	add_child(new_item)
	new_item.global_position = map_to_world(pos) + Vector2(6, 8)
	set_cellv(pos, -1)