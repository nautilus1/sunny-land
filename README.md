# Platformer Godot Game

<p align='center'>
  <img src="http://indielibre.com/wp-content/uploads/2019/05/agu.gif" url="https://indielibre.com"/>
  <img src="https://i.ibb.co/vLj3FwP/Deepin-Screenshot-dde-desktop-20190531142611.png" url="https://indielibre.com"/>
  <img src="http://indielibre.com/wp-content/uploads/2019/05/sec.gif" url="https://indielibre.com"/>
  <img src="https://i.ibb.co/Ldqd2vz/Deepin-Screenshot-dde-desktop-20190531142519.png" url="https://indielibre.com"/>
</p>

<p allign="center">
  This is a compilation of mechanics for Platformer Games, developvers on Godot Engine. For more information of the         repository: http://indielibre.com/platformer-game/.
</p>

-------

<p align='center'>
  <img src="http://indielibre.com/wp-content/uploads/2019/05/DeepinScreenshot_Seleccionar-%C3%A1rea_20190513150853-700x518.png"/>
</p>

This project tries to follow good practices for programming within Godot.

To guarantee the understanding of all the users, the GDScript style guide is followed: [Godot Docs](https://docs.godotengine.org/en/3.1/getting_started/scripting/gdscript/gdscript_styleguide.html)

## License GPL-v3

All the code of this repository is considering Free Software, you can download, learn, distribute and more. 

... But: the images and external Game Assets are part of other licenses, related with Itch.io and OpenGameArt. Some are Free for personal and comercial proyects.

You **can find Tutorials** for build each mechanic of the repository, here: http://indielibre.com/

## Dependencies

**Godot 3.1** or older. Download: https://godotengine.org/download

## Support

You can make donations or follow the development of new repositories and tutorials from my Patreon page: https://patreon.com/indielibre


[Indie Libre](https://indielibre.com/), **César León**.
